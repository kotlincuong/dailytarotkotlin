package com.example.dailytarotkotlin.base

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.dailytarotkotlin.R
import com.example.dailytarotkotlin.databinding.FragmentCardBinding
import com.example.dailytarotkotlin.model.CardRoom
import com.example.dailytarotkotlin.viewmodel.MainViewModel
import java.text.SimpleDateFormat
import java.util.*


open class BaseFragment : Fragment() {
    private lateinit var binding: FragmentCardBinding
    private lateinit var mMainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCardBinding.inflate(inflater)
        mMainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]

        return binding.root
    }

    fun setDataCard(it: CardRoom, indext: Int) {
        val c: Double = it.timeLoading
        val dateformat = SimpleDateFormat("hh:mma::EEE, d MMM yyyy", Locale(getLang()))
        val time = dateformat.format(c)

        if (time !== "") {
            val parts = time.split("::".toRegex()).toTypedArray()
            if (parts.size == 2) {
                val part1 = parts[0]
                when (indext) {
                    0 -> {
                        binding.tvCardPrediction.setText(R.string.tv_love)
                        binding.tvContentCard.text = it.contentTinhYeu
                        binding.tvTimeRead.text = part1.lowercase(Locale.getDefault())
                    }
                    1 -> {
                        binding.tvCardPrediction.setText(R.string.tv_synthetic)
                        binding.tvContentCard.text = it.contentTaiChinh
                        binding.tvTimeRead.text = part1.lowercase(Locale.getDefault())
                    }
                    2 -> {
                        binding.tvCardPrediction.setText(R.string.tv_work)
                        binding.tvContentCard.text = it.contentCongViec
                        binding.tvTimeRead.text = part1.lowercase(Locale.getDefault())
                    }
                    3 -> {
                        binding.tvCardPrediction.setText(R.string.tv_finance)
                        binding.tvContentCard.text = it.contentTongQuan
                        binding.tvTimeRead.text = part1.lowercase(Locale.getDefault())
                    }
                }


            }
        }
    }

    fun getLang(): String {
        val shareLang: SharedPreferences =
            requireActivity().getSharedPreferences("lang", Context.MODE_PRIVATE)
        val lang = shareLang.getString("lang", "en")
        Log.e("prox", "lang: " + lang)
        return lang.toString()
    }

}