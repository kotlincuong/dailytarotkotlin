package com.example.dailytarotkotlin.fragment.his

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.dailytarotkotlin.databinding.ItemHistoryBinding
import com.example.dailytarotkotlin.model.CardRoom
import java.text.SimpleDateFormat
import java.util.*

class HistoryAdapter(
    val context: Context,
    private var listHis: List<CardRoom>
) : RecyclerView.Adapter<HistoryAdapter.HisRadioViewHolder>() {

    private var clickListener: ((CardRoom) -> Unit)? = null

    fun setItemClickListener(listener: ((CardRoom) -> Unit)) {
        clickListener = listener
    }

    inner class HisRadioViewHolder(binding: ItemHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        internal val binding: ItemHistoryBinding

        init {
            this.binding = binding
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HisRadioViewHolder {
        val binding = ItemHistoryBinding.inflate(LayoutInflater.from(context), parent, false)
        return HisRadioViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HisRadioViewHolder, position: Int) {
        val item = listHis[position]

        with(holder) {
            val timeCard = item.timeLoading
            val dateformat = SimpleDateFormat("hh:mma::EEE, d MMM yyyy")
            val time = dateformat.format(timeCard)
            val parts = time.split("::".toRegex()).toTypedArray()
            if (parts.size == 2) {
                val part1 = parts[0]
                val part2 = parts[1]
                binding.tvTimeHistory.text = part1.lowercase(Locale.getDefault())
                binding.tvDateHistory.text = part2
            }

            binding.layoutHistory.setOnClickListener {
                clickListener?.let {
                    val time = java.lang.Double.valueOf(System.currentTimeMillis().toDouble())
                    val cardRoom = CardRoom(
                        item.id,
                        item.contentTinhYeu,
                        item.contentTongQuan,
                        item.contentCongViec,
                        item.contentTaiChinh,
                        time
                    )
                    it(cardRoom)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return listHis.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updaData(newList: List<CardRoom>) {
        listHis = newList
        notifyDataSetChanged()
    }


}