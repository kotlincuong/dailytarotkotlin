package com.example.dailytarotkotlin.fragment.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.example.dailytarotkotlin.R
import com.example.dailytarotkotlin.databinding.FragmentDetailsBinding
import me.relex.circleindicator.CircleIndicator3


class DetailsFragment : Fragment() {
    private lateinit var binding: FragmentDetailsBinding
    private lateinit var adapterCardViewPager: AdapterCardViewPager
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater)



        initEvent()
        initData()


        return binding.root
    }

    private fun initEvent() {
        binding.btNewPrediction.setOnClickListener {
            findNavController().navigate(R.id.action_detailsFragment_to_splashCreenFragment)
        }
        binding.imgArrowDown.setOnClickListener {
            findNavController().navigate(R.id.action_detailsFragment_to_historyFragment)
        }
        binding.imgSetting.setOnClickListener {
            findNavController().navigate(R.id.action_detailsFragment_to_settingFragment)
        }
    }

    private fun initData() {
        adapterCardViewPager = AdapterCardViewPager(requireActivity())
        // check ads ok = 4
        adapterCardViewPager.countCard = 5
        binding.viewPagerPrediction.adapter = adapterCardViewPager
        binding.cireslePrediction.setViewPager(binding.viewPagerPrediction)
    }


}