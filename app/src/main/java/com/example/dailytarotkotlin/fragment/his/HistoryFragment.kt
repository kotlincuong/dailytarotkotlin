package com.example.dailytarotkotlin.fragment.his

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bai1.database.CardDatabase
import com.example.bai1.viewmodel.CardRoomViewModel
import com.example.dailytarotkotlin.R
import com.example.dailytarotkotlin.api.APICardViewModel
import com.example.dailytarotkotlin.api.APIRepository
import com.example.dailytarotkotlin.api.APIViewModelFactory
import com.example.dailytarotkotlin.databinding.FragmentHistoryBinding
import com.example.dailytarotkotlin.model.CardRoom
import com.example.dailytarotkotlin.viewmodel.MainViewModel


class HistoryFragment : Fragment() {

    private lateinit var binding: FragmentHistoryBinding
    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var mMainViewModel: MainViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHistoryBinding.inflate(inflater)

        initView()
        initEvent()

        return binding.root
    }

    private fun initView() {
        mMainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        historyAdapter = HistoryAdapter(requireContext(), listOf())
        val layoutManagerVertical =
            GridLayoutManager(context, 1, LinearLayoutManager.VERTICAL, false)

        binding.rcHistoryCard.adapter = historyAdapter
        binding.rcHistoryCard.layoutManager = layoutManagerVertical


        activity?.let {
            mMainViewModel.readAllData.observe(it) {
                historyAdapter.updaData(it)
            }
        }

    }

    private fun initEvent() {

        binding.imgArrowUp.setOnClickListener {
            findNavController().navigate(R.id.action_historyFragment_to_detailsFragment)
        }

        historyAdapter.setItemClickListener {
            mMainViewModel.deleteBook(it)
            val card = CardRoom(
                0,
                it.contentTinhYeu,
                it.contentTaiChinh,
                it.contentCongViec,
                it.contentTongQuan,
                it.timeLoading
            )
            mMainViewModel.addBook(card)

            mMainViewModel.cardRoom.value = card
            requireActivity().findNavController(R.id.fragment_navigation).popBackStack()
        }

    }


}