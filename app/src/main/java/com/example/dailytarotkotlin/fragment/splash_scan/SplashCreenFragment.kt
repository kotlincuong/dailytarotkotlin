package com.example.dailytarotkotlin.fragment.splash_scan

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.bai1.database.CardDatabase
import com.example.dailytarotkotlin.R
import com.example.dailytarotkotlin.api.APICardViewModel
import com.example.dailytarotkotlin.api.APIRepository
import com.example.dailytarotkotlin.api.APIViewModelFactory
import com.example.dailytarotkotlin.base.BaseFragment
import com.example.dailytarotkotlin.databinding.ActivityMainBinding
import com.example.dailytarotkotlin.databinding.FragmentSplashCreemBinding
import com.example.dailytarotkotlin.model.CardRoom
import com.example.dailytarotkotlin.viewmodel.MainViewModel


class SplashCreenFragment : BaseFragment() {
    private lateinit var binding: FragmentSplashCreemBinding
    private lateinit var apiCardViewModel: APICardViewModel
    private lateinit var apiViewModelFactory: APIViewModelFactory

    private lateinit var mMainViewModel: MainViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashCreemBinding.inflate(inflater)

        initView()
        initEvent()



        return binding.root
    }


    private fun initView() {
        apiViewModelFactory = APIViewModelFactory(APIRepository())
        apiCardViewModel =
            ViewModelProvider(this, apiViewModelFactory)[APICardViewModel::class.java]
        mMainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
    }

    private fun initEvent() {
        binding.imgQuestionBig.setOnClickListener {
            getAPI(getLang())
        }
    }

    private fun getAPI(lang: String) {
        apiCardViewModel.getPostItem(lang)
        apiCardViewModel.responseValue.observe(viewLifecycleOwner) { res ->
            if (res.isSuccessful) {
                mMainViewModel.checkGetApi.value = true
                Log.e("prox", "successful")
                val cardApi = res.body()

                val time = java.lang.Double.valueOf(System.currentTimeMillis().toDouble())
                val cardRoom = CardRoom(
                    id = 0,
                    contentTinhYeu = cardApi!!.tinhYeu.content,
                    contentTongQuan = cardApi.tongQuan.content,
                    contentCongViec = cardApi.congViec.content,
                    contentTaiChinh = cardApi.taiChinh.content,
                    timeLoading = time
                )
                CardDatabase.getInstance(requireContext()).cardDao().addBook(cardRoom)

                mMainViewModel.cardRoom.value = cardRoom
                findNavController().navigate(R.id.action_splashCreenFragment_to_detailsFragment)
            } else {
                Toast.makeText(requireContext(), "error", Toast.LENGTH_LONG).show()
            }
        }
    }

}