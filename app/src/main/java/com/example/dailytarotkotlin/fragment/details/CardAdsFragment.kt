package com.example.dailytarotkotlin.fragment.details

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.dailytarotkotlin.R
import com.example.dailytarotkotlin.databinding.FragmentCardBinding
import com.example.dailytarotkotlin.viewmodel.MainViewModel
import java.text.SimpleDateFormat
import java.util.*


class CardAdsFragment : Fragment() {
    private lateinit var binding: FragmentCardBinding
    private lateinit var mMainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCardBinding.inflate(inflater)

        mMainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]

        initView()



        return binding.root
    }

    private fun initView() {
        binding.tvContentCard.visibility = View.GONE
        binding.tvTimeRead.visibility = View.GONE
        binding.tvCardPrediction.visibility = View.GONE
    }


}