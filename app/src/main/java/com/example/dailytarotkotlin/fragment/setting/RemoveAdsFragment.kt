package com.example.dailytarotkotlin.fragment.setting

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.dailytarotkotlin.databinding.FragmentRemoveAdsBinding


class RemoveAdsFragment : Fragment() {
    private lateinit var binding: FragmentRemoveAdsBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRemoveAdsBinding.inflate(inflater)
        // Inflate the layout for this fragment
        return binding.root
    }


}