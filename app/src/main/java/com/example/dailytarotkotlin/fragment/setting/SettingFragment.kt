package com.example.dailytarotkotlin.fragment.setting

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.dailytarotkotlin.R
import com.example.dailytarotkotlin.databinding.FragmentSettingBinding
import com.example.dailytarotkotlin.viewmodel.MainViewModel
import java.util.*


class SettingFragment : Fragment() {

    private lateinit var mMainViewModel: MainViewModel

    private lateinit var binding: FragmentSettingBinding
    private var ckSelectSetLang: Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSettingBinding.inflate(inflater)
        mMainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        initView()
        initEven()


        return binding.root
    }

    private fun initView() {
        setLangDefault()
    }


    private fun initEven() {
        ckSelectSetLang = false
        binding.imgClose.setOnClickListener {
            requireActivity().findNavController(R.id.fragment_navigation).popBackStack()
        }

        binding.layoutSetLang.setOnClickListener {
            ckSelectSetLang = !ckSelectSetLang
            if (ckSelectSetLang) {
                binding.layoutLang.visibility = View.VISIBLE
            } else {
                binding.layoutLang.visibility = View.GONE
            }
        }

        binding.langEn.setOnClickListener {
            checkLang("en")
        }
        binding.langWa.setOnClickListener {
            checkLang("wa")
        }
        binding.langPt.setOnClickListener {
            checkLang("pt")
        }
        binding.langKo.setOnClickListener {
            checkLang("ko")
        }
        binding.layoutRemoveAds.setOnClickListener {
            findNavController().navigate(R.id.action_settingFragment_to_removeAdsFragment)
        }

    }

    private fun setLangDefault() {
        when (getLang()) {
            "en" -> {
                binding.imgEnsign.setImageResource(R.drawable.ic_america)
                binding.textLangDefault.setText(R.string.tv_americ)
                binding.imgCheckEn.visibility = View.VISIBLE
                binding.imgCheckWa.visibility = View.GONE
                binding.imgCheckLt.visibility = View.GONE
                binding.imgCheckKo.visibility = View.GONE
            }
            "wa" -> {
                binding.imgEnsign.setImageResource(R.drawable.ic_belgium)
                binding.textLangDefault.setText(R.string.tv_belgium)
                binding.imgCheckWa.visibility = View.VISIBLE
                binding.imgCheckEn.visibility = View.GONE
                binding.imgCheckLt.visibility = View.GONE
                binding.imgCheckKo.visibility = View.GONE

            }
            "pt" -> {
                binding.imgEnsign.setImageResource(R.drawable.ic_brazil)
                binding.textLangDefault.setText(R.string.tv_brazil)
                binding.imgCheckLt.visibility = View.VISIBLE
                binding.imgCheckWa.visibility = View.GONE
                binding.imgCheckEn.visibility = View.GONE
                binding.imgCheckKo.visibility = View.GONE

            }
            "ko" -> {
                binding.imgEnsign.setImageResource(R.drawable.ic_south_korea)
                binding.textLangDefault.setText(R.string.tv_south_korea)
                binding.imgCheckKo.visibility = View.VISIBLE
                binding.imgCheckWa.visibility = View.GONE
                binding.imgCheckLt.visibility = View.GONE
                binding.imgCheckEn.visibility = View.GONE

            }
        }
    }

    private fun checkLang(idLang: String) {
        val sharedPreferences: SharedPreferences =
            requireActivity().getSharedPreferences("lang", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        if (idLang == getLang()) {
            binding.layoutLang.visibility = View.GONE
        } else {

            val locale = Locale(idLang)
            Locale.setDefault(locale)
            val resources = requireActivity().resources
            val configuration = Configuration()
            configuration.locale = locale
            resources.updateConfiguration(configuration, resources.displayMetrics)

            editor.putString("lang", idLang)
            editor.apply()
            editor.commit()
            mMainViewModel.checkSettingLang.value = true

//            findNavController().navigate(R.id.action_settingFragment_to_splashCreenFragment)
        }
        ckSelectSetLang = false
    }

    private fun getLang(): String {
        val shareLang: SharedPreferences =
            requireActivity().getSharedPreferences("lang", Context.MODE_PRIVATE)
        val lang = shareLang.getString("lang", "en")
        Log.e("prox", "lang: " + lang)
        return lang.toString()
    }


}