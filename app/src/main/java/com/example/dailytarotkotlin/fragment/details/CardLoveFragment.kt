package com.example.dailytarotkotlin.fragment.details

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.dailytarotkotlin.R
import com.example.dailytarotkotlin.base.BaseFragment
import com.example.dailytarotkotlin.databinding.FragmentCardBinding
import com.example.dailytarotkotlin.viewmodel.MainViewModel
import java.text.SimpleDateFormat
import java.util.*


class CardLoveFragment : BaseFragment() {
    private lateinit var binding: FragmentCardBinding
    private lateinit var mMainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCardBinding.inflate(inflater)

        mMainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]

        mMainViewModel.cardRoom.observe(requireActivity()) {
            if (it != null) {
                setDataCard(it, 0)
            }
        }
        return binding.root
    }

}