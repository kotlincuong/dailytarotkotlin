package com.example.dailytarotkotlin.fragment.details

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class AdapterCardViewPager(fa: FragmentActivity) : FragmentStateAdapter(fa) {
    var countCard = 0

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> CardLoveFragment()
            1 -> CardSyntheticFragment()
            2 -> CardWorkFragment()
            3 -> CardFinanceFragment()
            4 -> CardAdsFragment()
            else -> CardLoveFragment()
        }
    }

    override fun getItemCount(): Int {
        return countCard
    }
}
