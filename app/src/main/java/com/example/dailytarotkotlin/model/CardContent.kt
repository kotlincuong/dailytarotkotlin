package com.example.dailytarotkotlin.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CardContent(
    @SerializedName("type")
    @Expose
     var type: String,
    @SerializedName("image")
    @Expose
     val image: String,
    @SerializedName("content")
    @Expose
     val content: String
): Parcelable