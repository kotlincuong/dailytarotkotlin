package com.example.dailytarotkotlin.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CardApi(
    @SerializedName("TINHYEU")
    @Expose var tinhYeu: CardContent,
    @SerializedName("TONGQUAN")
    @Expose
    val tongQuan: CardContent,
    @SerializedName("CONGVIEC")
    @Expose
    val congViec: CardContent,
    @SerializedName("TAICHINH")
    @Expose
    val taiChinh: CardContent,
) : Parcelable