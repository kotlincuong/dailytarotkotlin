package com.example.dailytarotkotlin.model

import android.content.Intent
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "cardRoom")
data class CardRoom(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
     var contentTinhYeu: String,
     var contentTongQuan: String,
     var contentCongViec: String,
     var contentTaiChinh: String,
     var timeLoading: Double
) : Parcelable