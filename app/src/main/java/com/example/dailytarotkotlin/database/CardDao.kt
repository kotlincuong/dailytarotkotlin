package com.example.bai1.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dailytarotkotlin.model.CardRoom

@Dao
interface CardDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE) // <- Annotate the 'addUser' function below. Set the onConflict strategy to IGNORE so if exactly the same user exists, it will just ignore it.
    fun addBook(cardRoom: CardRoom)

    @Update
    fun updateBook(cardRoom: CardRoom)

    @Delete
    fun deleteBook(cardRoom: CardRoom)

    @Query("DELETE FROM cardRoom")
    fun deleteAllBook()

    @Query("SELECT * from cardRoom ORDER BY id DESC") // <- Add a query to fetch all users (in user_table) in ascending order by their IDs.
    fun readAllData(): LiveData<List<CardRoom>> // <- This means function return type is List. Specifically, a List of Users.

}