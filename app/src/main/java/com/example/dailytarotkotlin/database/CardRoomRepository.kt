package com.example.bai1.repository

import androidx.lifecycle.LiveData
import com.example.bai1.database.CardDao
import com.example.dailytarotkotlin.model.CardRoom

class CardRoomRepository(private val cardDao: CardDao) {
    val readAllData: LiveData<List<CardRoom>> = cardDao.readAllData()

    fun addBook(cardRoom: CardRoom) {
        return cardDao.addBook(cardRoom)
    }

    fun updateBook(cardRoom: CardRoom) {
        cardDao.updateBook(cardRoom)
    }

    fun deleteBook(cardRoom: CardRoom) {
        cardDao.deleteBook(cardRoom)
    }

    fun deleteAllBook() {
        cardDao.deleteAllBook()
    }
}