package com.example.bai1.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.dailytarotkotlin.model.CardRoom

@Database(entities = [CardRoom::class], version = 1)
abstract class CardDatabase : RoomDatabase() {
    companion object {
        private val DATABASE_NAME = "card_date_base.db"
        private var instance: CardDatabase? = null

        @Synchronized
        fun getInstance(context: Context): CardDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    CardDatabase::class.java,
                    DATABASE_NAME
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return instance!!
        }
    }

    abstract fun cardDao(): CardDao

}