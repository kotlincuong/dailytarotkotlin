package com.example.dailytarotkotlin.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.bai1.database.CardDatabase
import com.example.bai1.repository.CardRoomRepository
import com.example.dailytarotkotlin.model.CardRoom
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : AndroidViewModel(application) {
    var checkGetApi: MutableLiveData<Boolean> = MutableLiveData(false)
    var checkSettingLang: MutableLiveData<Boolean> = MutableLiveData(false)
    val cardRoom: MutableLiveData<CardRoom> = MutableLiveData()


    private var db = CardDatabase.getInstance(application)
    private var cardDao = db.cardDao()

    private val repository: CardRoomRepository = CardRoomRepository(cardDao)
    val readAllData = repository.readAllData

    fun addBook(cardRoom: CardRoom) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addBook(cardRoom)
        }
    }

    fun deleteBook(cardRoom: CardRoom) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteBook(cardRoom)
        }
    }

}