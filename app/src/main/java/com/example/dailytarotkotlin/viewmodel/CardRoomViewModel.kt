package com.example.bai1.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bai1.database.CardDatabase
import com.example.bai1.repository.CardRoomRepository
import com.example.dailytarotkotlin.api.APIRepository
import com.example.dailytarotkotlin.model.CardRoom
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CardRoomViewModel(application: Application) : AndroidViewModel(application) {

    private var db = CardDatabase.getInstance(application)
    private var cardDao = db.cardDao()

    private val repository: CardRoomRepository = CardRoomRepository(cardDao)
    val readAllData = repository.readAllData

    fun addBook(cardRoom: CardRoom) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addBook(cardRoom)
        }
    }

    //
//    fun updateBook(book: Book) {
//        viewModelScope.launch(Dispatchers.IO) {
//            repository.updateBook(book)
//        }
//    }
//
    fun deleteBook(cardRoom: CardRoom) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteBook(cardRoom)
        }
    }
//
//    fun deleteAllUsers() {
//        viewModelScope.launch(Dispatchers.IO) {
//            repository.deleteAllBook()
//        }
//    }
}


