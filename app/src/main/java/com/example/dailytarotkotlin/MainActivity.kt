package com.example.dailytarotkotlin

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.dailytarotkotlin.databinding.ActivityMainBinding
import com.example.dailytarotkotlin.viewmodel.MainViewModel
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var mNavController: NavController
    private lateinit var mMainViewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLanguage(this)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initEvent()


    }

    private fun initView() {
        mMainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        mMainViewModel.checkSettingLang.observe(this) {
            if (it) {
                restartActivity()
                mMainViewModel.checkSettingLang.value = false
            }
        }
    }

    private fun initEvent() {

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_navigation) as NavHostFragment
        mNavController = navHostFragment.navController

    }

    // Enable back arrow button functionality in Add Fragment to return to List Fragment (main page of the app)
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment_navigation)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if (mNavController.currentDestination?.id == R.id.detailsFragment) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    fun getLanguage(activity: Activity?) {
        val shareLang: SharedPreferences =
            activity!!.getSharedPreferences("lang", Context.MODE_PRIVATE)
        val lang = shareLang.getString("lang", "en")

        val locale = Locale(lang)
        Locale.setDefault(locale)
        val resources = activity!!.resources
        val configuration = Configuration()
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
    }

    fun restartActivity() {
        val mIntent = intent
        finish()
        startActivity(mIntent)
    }


}