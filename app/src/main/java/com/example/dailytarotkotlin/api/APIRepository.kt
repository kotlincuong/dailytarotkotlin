package com.example.dailytarotkotlin.api

import com.example.dailytarotkotlin.model.CardApi
import retrofit2.Response

class APIRepository {
    suspend fun getPostItem(lang: String): Response<CardApi> {
        return RetrofitInstance.invoke().getApiCard(lang)
    }
}