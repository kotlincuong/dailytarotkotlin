package com.example.dailytarotkotlin.api

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class APIViewModelFactory(private val repos: APIRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return APICardViewModel(repos) as T
    }
}