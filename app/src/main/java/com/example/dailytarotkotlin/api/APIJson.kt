package com.example.dailytarotkotlin.api

import com.example.dailytarotkotlin.model.CardApi
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface APIJson {
    // http://178.79.156.246:6651/tarot/generate?lang=en
    @GET("tarot/generate")
    suspend fun getApiCard(
        @Query("lang") lang: String
    ): Response<CardApi>

}