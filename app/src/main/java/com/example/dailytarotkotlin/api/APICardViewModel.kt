package com.example.dailytarotkotlin.api

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dailytarotkotlin.model.CardApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class APICardViewModel(private val respos: APIRepository) : ViewModel() {
    val responseValue: MutableLiveData<Response<CardApi>> = MutableLiveData()

    fun getPostItem(lang: String) {
        viewModelScope.launch {
            responseValue.value = respos.getPostItem(lang)
        }
    }


}